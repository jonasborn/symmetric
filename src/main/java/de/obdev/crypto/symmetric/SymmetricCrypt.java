package de.obdev.crypto.symmetric;


import org.bouncycastle.jce.provider.BouncyCastleProvider;

import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.Security;
import java.security.spec.InvalidKeySpecException;

/**
 * Created by jonas on 01.02.17.
 */
public class SymmetricCrypt {

    static {
        Security.addProvider(new BouncyCastleProvider());
    }

    String cipher;
    SymmetricKey key;
    String keyType;

    Cipher encode;
    Cipher decode;


    /**
     * Create new cipher
     * @param key A symmetric key, containing key and iv
     * @param cipher A cipher accessible by java
     * @param keyType A key type
     * @throws InvalidKeySpecException
     * @throws InvalidAlgorithmParameterException
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeyException
     * @throws NoSuchPaddingException
     * @throws UnsupportedEncodingException
     */
    public SymmetricCrypt(SymmetricKey key, String cipher, String keyType) throws InvalidKeySpecException, InvalidAlgorithmParameterException, NoSuchAlgorithmException, InvalidKeyException, NoSuchPaddingException, UnsupportedEncodingException {
        this.cipher = cipher;
        this.key = key;
        this.keyType = keyType;
        this.encode = createCipher(Cipher.ENCRYPT_MODE);
        this.decode = createCipher(Cipher.DECRYPT_MODE);
    }

    /**
     *
     * @param mode Mode for the new cipher
     * @return A new cipher
     * @throws NoSuchPaddingException
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeySpecException
     * @throws InvalidAlgorithmParameterException
     * @throws InvalidKeyException
     * @throws UnsupportedEncodingException
     */
    public Cipher createCipher(Integer mode) throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeySpecException, InvalidAlgorithmParameterException, InvalidKeyException, UnsupportedEncodingException {
        Cipher cipher = Cipher.getInstance(this.cipher);
        cipher.init(mode, key.getKeySpec(this.keyType), new IvParameterSpec(key.getIvBytes()));
        return cipher;
    }

    /**
     * Encrypts something with the given key
     * @param input Any byte array
     * @return
     * @throws Exception
     */
    public byte[] encrypt(byte[] input) throws Exception {
        return encode.doFinal(input);
    }

    /**
     * Decrypts a encrypted array with the given key
     * @param input A byte array with encrypted data
     * @return
     * @throws Exception
     */
    public byte[] decrypt(byte[] input) throws Exception {
        return decode.doFinal(input);
    }
}
