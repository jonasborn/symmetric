package de.obdev.crypto.symmetric;

import com.google.common.hash.Hashing;
import com.google.common.io.BaseEncoding;
import de.obdev.util.random.Random;

import javax.crypto.spec.SecretKeySpec;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

/**
 * Created by jonas on 01.02.17.
 */
public class SymmetricKey {

    static Integer KEY_LENGTH = 32;
    static Integer IV_LENGTH = 16;

    private String key;
    private String ivBytes;

    /**
     * Creates a new SymmetricKey based on two byte arrays
     * @param key A key as a byte array with a length of a multiple of 8
     * @param iv A key as a byte array with a length of 16
     * @throws UnsupportedEncodingException
     */
    public SymmetricKey(byte[] key, byte[] iv) throws UnsupportedEncodingException {
        this.key = BaseEncoding.base64().encode(key);
        this.ivBytes = BaseEncoding.base64().encode(iv);
    }

    /**
     * Creates a new SymmetricKey based on a byte array
     * @param key A key as a byte array with a length of a multiple of 8
     * @throws UnsupportedEncodingException
     */
    public SymmetricKey(byte[] key) throws UnsupportedEncodingException {
        this.key = BaseEncoding.base64().encode(key);
        ivBytes = new String(Random.nextBytes(IV_LENGTH));
    }

    /**
     * Creates a new secure random SymmetricKey
     */
    public SymmetricKey() {
        this.key = BaseEncoding.base64().encode(Random.nextBytes(KEY_LENGTH));
        ivBytes = BaseEncoding.base64().encode(Random.nextBytes(IV_LENGTH));
    }

    /**
     * Returns the key part as Base64 encoded string
     * @return {String}
     */
    public String getKey() {
        return key;
    }

    /**
     * Return the iv part as Base64 encoded string
     * @return {String}
     */
    public String getIv() {
        return ivBytes;
    }

    public byte[] getKeyBytes() {
        return BaseEncoding.base64().decode(key);
    }

    public byte[] getIvBytes() {
        return BaseEncoding.base64().decode(ivBytes);
    }

    public SecretKeySpec getKeySpec(final String cipher) throws InvalidKeySpecException, NoSuchAlgorithmException, UnsupportedEncodingException {
        return new SecretKeySpec(getKeyBytes(), cipher);
    }

}
