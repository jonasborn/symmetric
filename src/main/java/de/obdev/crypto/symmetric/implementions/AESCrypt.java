package de.obdev.crypto.symmetric.implementions;


import de.obdev.crypto.symmetric.SymmetricKey;
import de.obdev.crypto.symmetric.SymmetricCrypt;

import javax.crypto.NoSuchPaddingException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

/**
 * Created by obdev on 10.01.2017.
 */
public class AESCrypt extends SymmetricCrypt {

    public AESCrypt(SymmetricKey key) throws NoSuchAlgorithmException, InvalidAlgorithmParameterException, InvalidKeySpecException, InvalidKeyException, NoSuchPaddingException, UnsupportedEncodingException {
        super(key,  "AES/CBC/PKCS5Padding", "AES");
    }

}
