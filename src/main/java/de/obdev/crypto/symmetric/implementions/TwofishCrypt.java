package de.obdev.crypto.symmetric.implementions;



import de.obdev.crypto.symmetric.SymmetricCrypt;
import de.obdev.crypto.symmetric.SymmetricKey;

import javax.crypto.NoSuchPaddingException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

/**
 * Created by jonas on 01.02.17.
 */
public class TwofishCrypt extends SymmetricCrypt {


    public TwofishCrypt(SymmetricKey key) throws NoSuchAlgorithmException, InvalidAlgorithmParameterException, InvalidKeySpecException, InvalidKeyException, NoSuchPaddingException, UnsupportedEncodingException {
        super(key, "twofish", "twofish");
    }


}
